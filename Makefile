CC = gcc
CFLAGS = -fcommon -s
LDFLAGS = -lpcap

all:
	$(CC) $(CFLAGS) -o hexinject hexinject.c $(LDFLAGS) 
	$(CC) $(CFLAGS) -o prettypacket prettypacket.c $(LDFLAGS) 
	$(CC) $(CFLAGS) -o hex2raw hex2raw.c $(LDFLAGS) 

clean:
	rm -f hexinject prettypacket hex2raw *~
